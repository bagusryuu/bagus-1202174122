<?php

$connect = mysqli_connect('localhost','root','','db_wad_04');
if (!$connect) {
    die("Connection failed: " . mysqli_connect_error());
}
session_start();


?>


<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script
    src="https://code.jquery.com/jquery-3.4.1.js"
    integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
    crossorigin="anonymous"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
    <title>home</title>
  </head>
  <body>
    
    <nav class="navbar fixed-top navbar-light navbar-expand-lg navbar-template bg-light text-dark">
    <div class="collapse navbar-collapse order-3 order-lg-2" id="navbarNavDropdown">
    <img src="EAD.png" alt="" width=200px; height=50px>
              <ul class="navbar-nav ml-auto">
                  
              <li class="nav-item"><a class="nav-link px-2" href="cart.php"><span class="fa fa-shopping-cart"></span></a></li>
              <div class="btn-group">
              <li style="margin-right: 70px;" class="nav-link px-2 dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <?php echo $_SESSION['username']; ?>
              </li>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="editprofile.php">Profile</a>
                <a class="dropdown-item" href="home1.php">logout</a>
              

          </div>
        </nav>

    <div class="judul">
    <div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h1 class="display-4">Hello Coders</h1>
    <p class="lead">Welcome to our store, please take a look for the product you might buy</p>
  </div>
</div>
</div>

<div class="menu">

<div class="card-deck">
  <div class="card">
    <img class="card-img-top" src="web_fundamental_logo_030519090933.jpg" alt="Card image cap">
    <div class="card-body">
      <h5 class="card-title">Belajar Dasar Pemrograman Web</h5>
      <p>Rp.210.000,-</p>
      <p class="card-text">Ingin bisa membuat website? Pelajari komponen dasar seperti HTML, CSS dan JavaScript pada kurikulum kelas ini.</p>
    </div>
    <div class="card-footer">
    <a href="coba.php?product=Belajar+Dasar+Pemrograman+Web&price=210"name="but" class="btn btn-primary btn-block">Buy</a>
    </div>
  </div>
  <div class="card">
    <img class="card-img-top" src="java_fundamental_logo_080119141920.jpg" alt="Card image cap">
    <div class="card-body">
      <h5 class="card-title">Memulai Pemrograman Dengan Java</h5>
      <p>Rp.150.000,-</p>
      <p class="card-text">Belajar Bahasa Java buat kamu yang ingin mempelajari konsep Pemrograman Berorientasi Objek (PBO) terpopuler untuk mengembangkan aplikasi.</p>
    </div>
    <div class="card-footer">
    <a href="coba.php?product=Memulai+Pemrograman+Dengan+Java&price=150" name="but" class="btn btn-primary btn-block">Buy</a>
    </div>
  </div>
  <div class="card">
    <img class="card-img-top" src="memulai_pemrograman_dengan_python_logo_090719134021.jpg" alt="Card image cap">
    <div class="card-body">
      <h5 class="card-title">Memulai Pemrograman Dengan Python</h5>
      <p>Rp.200.000,-</p>
      <p class="card-text">Belajar Bahasa Python - Fundamental berbagai tren Industri saat ini : Data Science, Machine Learning, Infrastructure-management</p>
    </div>
    <div class="card-footer">
    <a href="coba.php?product=Memulai+Pemrograman+Dengan+Python&price=200" name="but" class="btn btn-primary btn-block">Buy</a>
    </div>
  </div>
</div>

<nav class="navbar fixed-bottom navbar-light navbar-expand-lg navbar-template bg-light text-dark">
<div class="bawah"> 
  <a class="navbar-brand text-center" href="#">© EAD STORE</a>
</nav>
</div>

<!--modal login boostrap-->
<div class="modal hide fade" id="login" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
        <h4 class="modal-title">Login</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
        <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Email Address:</label>
            <input type="text" class="form-control" placeholder="email" id="email" required>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label" >Password:</label>
            <input type="password" class="form-control" placeholder="password" id="password" required>
          </div>
        </form>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" >Login</button>
        </div>
      </div>
      
    </div>
  </div>
</div>  
  
  
  <!--modal Register boostrap-->
  <div class="container">
  <div class="modal hide fade" id="regis" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
        <h4 class="modal-title">Login</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
        <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Email Address:</label>
            <input type="email" class="form-control" placeholder="email" id="email1" required>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Username:</label>
            <input type="text" class="form-control" placeholder="username" id="username" required>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Password:</label>
            <input type="password" class="form-control" placeholder="password" id="password1" required>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Confirm Password:</label>
            <input type="password" class="form-control" placeholder="confirm password" id="password2" required>
          </div>
        </form>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" >Register</button>
        </div>
      </div>
      
    </div>
  </div>

  </div>



</body>


</html>
<style>
.judul {
    width: 75%;
    height: auto;
    margin-left: auto;
    margin-right: auto;
    padding: 10px;
    
    

    min-height: 170px;
}
.menu {
    width: 75%;
    height: auto;
    margin-left: auto;
    margin-right: auto;
    padding: 10px;
    

    min-height: 170px;
}
.bawah {
    margin-left: auto;
    margin-right: auto;
    left: 0;
    right: 0;  
    font-size: 20px;
}
.lead {
    font-size: 18px;
}
body {
    padding: 100px;
}

</style>