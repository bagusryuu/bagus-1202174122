<?php
    $nama = $_GET['nama'];
    $telepon = $_GET['telepon'];
    $date = $_GET['date'];
    $inlineRadioOptions = $_GET['inlineRadioOptions'];
    

    ?>
<html>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <body>
      

    <div class="d-md-flex h-md-10 align-items-center">

<div class="col-md-6 p-0">
    <div class="text-black d-md-flex h-100 p-5 text-center position-fixed" id="left">

        <div class="jumbotron-fluid">
            <div class="container">

              <h1 class="display-4 text-center">~Data Driver Ojol~</h1>
              <br>
              <label class="col-sm-3 col-form-label font-weight-bold">Nama</label>
            <br><?= $nama ?><br><br>
              <label class="col-sm-5 col-form-label font-weight-bold">Nomor Telepon</label>
            <br><?= $telepon ?><br><br>
              <label class="col-sm-3 col-form-label font-weight-bold">Tanggal</label>
            <br><?= $date ?><br><br>
              <label class="col-sm-3 col-form-label font-weight-bold">Asal Driver</label>
            <br><?= $inlineRadioOptions ?><br><br>
              <label class="col-sm-5 col-form-label font-weight-bold">Bawa Kantong</label>
            <br>
            <?php
            $isChecked = !empty($_GET["check"]) ? true : false;
            if($isChecked){
                echo 'Bawa';
            }
            else{
                echo 'Tidak';
            }
            ?> <br><br><br>

            <div class= "text-center">
            <button onclick="location.href='form.html'" class="btn btn-primary"><< KEMBALI</button>


            </div>
          </div>
        </div>
        
    </div>
</div>

<div class="col-md-6 p-0 bg-white h-md-10">
    <div class="d-md-flex h-md-100 p-5 justify-content-center">
    <div class="logoarea pt-3 pb-3">
        <div class="jumbotron-fluid">
            <div class="container">
              <h1 class="display-4 text-center">~Menu~</h1>
              <br>
              <p class="lead text-center">Pilih Menu</p>
            </div>
            <form action="nota.php" method="POST" style="margin: 30px auto; width: 350px" onsubmit="return confirm('Apakah anda yakin?');" >
            <div class="form-group row">
            <div class="container py-1 mt-3">
    <div class="row form-check">
        <div class="col-6"><input class="form-check-input position-static" type="checkbox" name="check[]" value="1"> Es Coklat Susu</div>
        <div class="col-12"><span class="float-right">Rp. 28.000,-</span></div>
    </div>
    <br>
    <hr>
          </div>

    </div>
    <div class="row form-check">
        <div class="col-6"><input class="form-check-input position-static" type="checkbox" name="check[]" value="1"> Es Susu Matcha</div>
        <div class="col-12"><span class="float-right">Rp. 18.000,-</span></div>
    </div><br>
    <hr>
    <div class="row form-check">
        <div class="col-6"><input class="form-check-input position-static" type="checkbox" name="check[]" value="1"> Es Susu Mojicha</div>
        <div class="col-12"><span class="float-right">Rp. 15.000,-</span></div>
    </div><br>
    <hr>
    <div class="row form-check">
        <div class="col-6"><input class="form-check-input position-static" type="checkbox" name="check[]" value="1"> Es Matha latte</div>
        <div class="col-12"><span class="float-right">Rp. 30.000,-</span></div>
    </div><br>
    <hr>
    <div class="row form-check">
        <div class="col-6"><input class="form-check-input position-static" type="checkbox" name="check[]" value="1"> Es Taro Susu</div>
        <div class="col-12"><span class="float-right">Rp. 21.000,-</span></div>
    </div><br>
    <hr>
    
                            <div class="form-group row">
                              <label class="col-sm-4 col-form-label">Nomor Order</label>
                              <div class="col-sm-8">
                                <input type="number" class="form-control" name="order" required>
                              </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                              <label class="col-sm-4 col-form-label">Nama Pemesan</label>
                              <div class="col-sm-8">
                                <input type="text" class="form-control" name="pesan" required>
                              </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                              <label class="col-sm-4 col-form-label">Email</label>
                              <div class="col-sm-8">
                                <input type="email" class="form-control" name="email" required>
                              </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                              <label class="col-sm-4 col-form-label">Alamat</label>
                              <div class="col-sm-8">
                                <input type="text" class="form-control" name="alamat" required>
                              </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Member</label>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="radio" id="inlineRadio1" value="ya" required>
                                        <label class="form-check-label" for="inlineRadio2">Ya</label>
                                      </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="radio" id="inlineRadio2" value="tidak" required>
                                        <label class="form-check-label" for="inlineRadio2">Tidak</label>
                                      </div>
                                  </div>
                            <hr>
                            <div class="form-group row">
                              <label class="col-sm-4 col-form-label">Metode Pembayaran</label>
                              <div class="col-sm-8">
                              <select class="form-control" name="paymentSelection" required>
                              <option value="" selected disabled hidden>Choose here:</option>
                              <option value="Cash">Cash</option>
                              <option value="E-Money (GoPay/OVO)">E-Money (GoPay/OVO)</option>
                              <option value="Credit Card">Credit Card</option>
                              <option value="Lainnya">Lainnya</option>`
                            </select>
                            <hr>  
                              
                          </div>
                          </div>
                          <div class= "text-center">
                              <button type="submit" id="button3" name="but" class="btn btn-primary btn-lg btn-block">CETAK STRUK</button>  
                      </div>
                      </div>
    
                      </div>
                      </div>
                      </div>
                      </div>
                      </div>
          </form>
    </body>
</html>

<style>

@media (min-width: 768px) {
    .h-md-100 { height: 1vh; }
}
.btn-round { border-radius: 30px; }
.bg-indigo { background: white; }
.text-cyan { color: #35bdff; }
body, html {
    height: 100%;
}

#left {
    
    margin-left : 100px;
    margin-top : -30px;
}
</style>