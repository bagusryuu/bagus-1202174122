@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <div class="card">
                <div class="card-header">
                    <img src="https://lh3.googleusercontent.com/baCx0_2NsKJF8shRUMZTXVhrnMna-y86kxoIgVT8Hb-PpiT1kv8XN4xbl1ucFqZ3BTrvuf70fWIIpYKQJy2XhB_fYDaHPRleVn3FlPUCTvbeDmrgkZ4h7RKTQKXusCOnwQuN8tPUAQ=w745-h993-no" alt="" style="width : 50px; height: 50px;" class="rounded-circle"> &nbsp;{{ Auth::user()->name }}</div>

                <div class="card-body">
                    <img src="https://lh3.googleusercontent.com/MNEfzEV7FTAFDvCD8rxg0HVUtzRsGa9FUwJY-39QA1hJ3EO9vMjCi5JI_9l95QhPpd1YTbCaR_2Am6fa-Wjr4By6kIfptDVw3x5dasY43vTVjJyaMB3RAkOMSGdOqdzx1iRvBIaGNbc=w1920-h542-no" alt="Logo EAD" style="width: 500px;">
                </div>
                <div class="card-footer">
                <b>{{ Auth::user()->email }}</b>
                <br>
                Logo EAD
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <div class="card">
                <div class="card-header">
                    <img src="https://lh3.googleusercontent.com/baCx0_2NsKJF8shRUMZTXVhrnMna-y86kxoIgVT8Hb-PpiT1kv8XN4xbl1ucFqZ3BTrvuf70fWIIpYKQJy2XhB_fYDaHPRleVn3FlPUCTvbeDmrgkZ4h7RKTQKXusCOnwQuN8tPUAQ=w745-h993-no" alt="" style="width : 50px; height: 50px;" class="rounded-circle"> &nbsp;{{ Auth::user()->name }}</div>

                <div class="card-body">
                    <img src="https://lh3.googleusercontent.com/BKvgTnL4gVk42gYtJW6hAcpjqgcC-HYEIfY48xCJow4nP7AGiFb389Sxnej9DJ35IuN336nsfkTpUvm_KKv5BIMgFK650iyF050ch29qzd83Fy3S_fXFJwmXXTIHsIZ5YE2zo5WkHg=w1072-h790-no" alt="Logo EAD" style="width: 500px;">
                </div>
                <div class="card-footer">
                <b>{{ Auth::user()->email }}</b>
                <br>
                MEME Kuliah
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
