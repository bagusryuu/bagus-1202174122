@extends('layouts.app')

@section('content')
<br><br><br>
<div class="container">
    <div class="text-center">
        <div class="row">
            <div class="col">
                <img src="{{ url('/profile1/'.Auth::user()->avatar) }}" alt="" style="width: 250px" class="rounded-circle">
            </div>
            <div class="col">
                <h3 style="text-align:left;">{{ Auth::user()->name}}</h3>
                <br>
                <div class="row text-center">
                    <a href="/editProfile">Edit Profile</a>
                </div>
                <div class="row">
                    <b>{{ $posts->count()}}</b>&nbsp; posts
                </div>
                <br>
                <div class="row">
                    <b>{{ Auth::user()->title }}</b>
                </div>
                <div class="row">
                    {{ Auth::user()->description }}
                </div>
                <div class="row">
                    <a href="https://{{ Auth::user()->url }}">{{ Auth::user()->url }}</a>
                </div>
            </div>
            <div class="col">
                <a href="/addNewPost">Add New Post</a>
            </div>
        </div>
        <hr>
        <div class="card-deck">
            @foreach($posts as $p)
            <div class="card">
                <img width="auto" class="img-fluid" src="{{ url('/img/'.$p->image) }}" alt="foto">
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection