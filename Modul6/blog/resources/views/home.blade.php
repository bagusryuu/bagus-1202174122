@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @foreach($post->reverse() as $p)
            <form action="/detailPost" method="get">
                <div class="card">
                    <div class="card-header">
                        <!-- avatar -->
                        <!-- <img src="https://lh3.googleusercontent.com/SN5YPRmPRASjOqQmu3-zLZXDi1QToAkVBv5HmS4e1FMvdt3r98O_GsFR5v43trJZngYWPZncWBLRkIMUkvPkQvE1KSEdMG1CsdFaJT-X1O5itT2jLn_udpx10vcVhLqipGB6UsC8dWI=s888-no" alt="" style="width : 50px;" class="rounded-circle"> &nbsp;{{ Auth::user()->name }}</div> -->
                        <a class="text-center" href="/profile"><img src="{{ url('/profile1/'. Auth::user()->avatar) }}"  alt="" style="width : 50px;" class="rounded-circle"> &nbsp;{{ Auth::user()->name }}</div></a>

                    <div class="card-body">
                        <!-- <a href="/home/detail"><img src="sample_post.jpg" alt="Logo EAD" style="width: 100%;"></a> -->
                        <a class="text-center" href="/detailPost/{{ $p->id }}"><img class="img-fluid" style="width: auto;" src="{{ url('/img/'.$p->image) }}" alt="asd"></a>
                    </div>
                    <div class="card-footer">
                        <div>
                    <span class="fa fa-heart"> &nbsp; &nbsp;<span class="far fa-comment"></span>
                    </div>
                    <br>
                        <b>{{ $p->likes }} Likes</b></span></a></li> <br>
                        <b>{{ Auth::user()->email }}</b> {{ $p->caption }}
                        <br>
                        <div class="form-group form-inline">
                            <input class="form-control" style="width: 92%;" type="text" name="comment" placeholder="Masukan Komentar..">
                            <input type="submit" value="Post" class="btn btn-outline-primary">
                        </div>

                    </div>

                </div>
                <br>
                <br>
            </form>
            @endforeach

@endsection
