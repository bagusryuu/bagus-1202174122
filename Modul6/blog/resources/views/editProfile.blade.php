@extends('layouts.app')

@section('content')

<form action="/editProfile" method="post" enctype="multipart/form-data"> 
@csrf
<div class="row justify-content-center">
<div class="col-md-6">
<h1 class="display-6">Edit Profile</h1>
  <div class="form-group">
    <label for="exampleInputEmail1">Title</label>
    <input type="text" class="form-control" id="title" name="title" aria-describedby="emailHelp" placeholder="">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Description</label>
    <input type="text" class="form-control" id="desc" name="desc" aria-describedby="emailHelp" placeholder="">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">URL</label>
    <input type="text" class="form-control" id="url" name="url" aria-describedby="emailHelp" placeholder="">
  </div>
  <div class="form-group">
    <label for="exampleFormControlFile1">Post Image</label>
    <input type="file" name="avatar" class="form-control-file" id="image">
  </div>
  <button type="submit" class="btn btn-primary">Save Profile</button>
  </div>
  </div>
</form>

@endsection