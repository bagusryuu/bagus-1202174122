@extends('layouts.app')

@section('content')

<form action="/addNewPost" method="post" enctype="multipart/form-data">
<div class="row justify-content-center">
<div class="col-md-6">
        <h1 class="display-4">Add New Post</h1>
        <div class="form-group">
            <label for="exampleInputEmail1">Post Caption</label>
            <input type="text" class="form-control" name="caption" id="caption" aria-describedby="emailHelp" placeholder="Input Caption">
        </div>
        @csrf
        
  <div class="form-group">
    <label for="exampleFormControlFile1">Post Image</label>
    <input type="file" name="image" class="form-control-file" id="image">
  </div>
  <button type="submit" class="btn btn-primary">Add New Post</button>
</form>

@endsection