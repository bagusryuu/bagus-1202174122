@extends('layouts.app')

@section('content')

<div class="container">
  <div class="row">
    <div class="col-sm-8"><img src="{{ $post->image }}" class="img-fluid"></div>
    <div class="col-sm-4">
      <img src="{{ url('/Assets/'.$posts->users->avatar) }}" class="rounded-circle" style="width: 12%; margin-right: 4%;"> <b>{{ $post->users->name }}</B>
      <hr>
      test
      <br>
      
      @foreach($posts->komentar_post as $komen)
      <p>
        <b>{{ $komen->users->email }}</b> {{ $komen->comment }}
      </p>
      @endforeach
      <hr>
      <form action="/detailPost" method="POST" style="display:inline">
        @csrf
        <button type="submit" name="like" class="btn" value="{{ $posts->id }}"><i class="far fa-heart"></i></button>
      </form>
      <button type="button" class="btn"><i class="far fa-comment"></i></button>
      <br>
      
      <br>
      <p><b>{{ $posts->likes }}</b> Likes</p>
      <form action="/tambah_komen" method="post">
        @csrf
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="Masukan Komentar" name="komentar">
          <div class="input-group-append">
            <button class="btn" type="submit" name="button_komen" value="{{ $posts->id }}"></button>
          </div>
        </div>
      </form>

      
    </div>
  </div>
</div>

@endsection