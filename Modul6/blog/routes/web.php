<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/home', 'HomeController@index');
Route::get('/addNewPost', 'PostController@show');
Route::post('/addNewPost','PostController@tambah1');
Route::get('/editProfile', 'UserController@editProfile');
Route::post('/editProfile', 'UserController@editProfile1');
Route::get('/detailPost/{id}', 'DetailController@detailPost');
Route::get('/profile', 'ProfileController@profile');
