<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Auth;

use App\Posts;

class DetailController extends Controller
{
    public function detailPost($id) 
    {
        $posts = Posts::where('id',$id)->first();

        return view ('detailPost', ['posts'=>$posts]);
        
    }
}
