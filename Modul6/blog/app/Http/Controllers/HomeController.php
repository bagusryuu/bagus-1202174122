<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Posts;
use App\User;
use App\komentar_posts;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $user = User::all();
        $posts = posts::all();
        
        
        return view('home',['user'=>$user, 'post' => $posts]);
    }

    public function detail()
    {
        $user = User::all();
        $posts = posts::find($id);
        $komentar = komentar_posts::all();
        
        return view('detailPost',['user'=>$user, 'post' => $posts, 'komentar_posts' => $komentar]); 
    }



}
?>
