<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Posts;

class ProfileController extends Controller
{
    public function profile()
    {
        $posts = Posts::get();

        return view('profile', ['posts'=>$posts]);
    }
}
