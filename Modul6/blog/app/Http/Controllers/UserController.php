<?php

namespace App\Http\Controllers;

use Auth;

use App\User;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function editProfile(Request $request)
    {
        return view('editProfile');
    }
    public function editProfile1(Request $request)
    {
        $user = Auth::user();
        if(is_null($user)) redirect('/');
        $file=$request->file('avatar');
        // $file->storeAs('profile',$file->getClientOriginalName());
        $file->move('profile1',$file->getClientOriginalName());

        User::find($user->id)->update ([
            'title'=>$request->title,
            'description'=>$request->desc,
            'url'=>$request->url,
            'avatar'=>$file->getClientOriginalName()
        ]);

        return redirect('/home');
    }
}
