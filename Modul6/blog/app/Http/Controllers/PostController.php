<?php

namespace App\Http\Controllers;

use Auth;

use App\Posts;

use Illuminate\Http\Request;

class PostController extends Controller
{
    public function show(Request $request) 
    {
        return view('addNewPost');
    }
    
    public function tambah1(Request  $request)
    {
        $user = Auth::user();
        if(is_null($user)) redirect('/');
        $file=$request->file('image');
        $file->move('img',$file->getClientOriginalName());

        $post = Posts::create ([
            'user_id'=>$user->id,
            'caption'=>$request->caption,
            'image'=>$file->getClientOriginalName()
        ]);

        return redirect('/home');
    }
}
