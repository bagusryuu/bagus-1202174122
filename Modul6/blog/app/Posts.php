<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    protected $table = "posts";
    protected $fillable = [
        'caption','image','user_id','like'
    ];
    public function komentar()
    {
        return $this->hasMany('App\komentar_posts', 'post_id', 'id');
    }
    public function user()
    {
        return $this->hasOne('App\User');
    }

}
